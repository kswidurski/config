# config

Repo with my "dot files", used both in my linux and windows machines.
Some scripts or ideas are shamelessly stolen from other people.

## Usage
Clone this repo:
```
git clone https://gitlab.com/kswidurski/config.git
```
Then add "exectue" permission and exacute *install.sh* script:
```
chmod +x install.sh
./install.sh
```
This will display a couple of questions allowing you to configure your .bashrc

![Execution of the install.sh script](screenshot.png)



