#!/bin/bash


RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color


PLATFORM=`uname`
if [[ "$PLATFORM" == *"Linux"* ]]; then
	IS_LINUX=true
fi

INSTALL_APPS=${IS_LINUX=false}
APPS=("starship" "btop" "fzf" "vim")

declare -A CONFIG_FILES_TO_INSTALL=()


# Ask Y/n
function ask() {
	echo -en "$1 (${GREEN}Y${NC}/${RED}n${NC}): " 
	read resp
	if [ -z "$resp" ]; then
		response_lc="y" # empty is Yes
	else
		response_lc=$(echo "$resp" | tr '[:upper:]' '[:lower:]') # case insensitive
	fi

	[ "$response_lc" = "y" ]
}

function include() {
	echo -e "Do you want include files from '${BLUE}${1}${NC}':"
	for file in $1/*; do
		if [ -f "$file" ]; then
			filename=$(basename "$file")
			if ask "- ${YELLOW}${filename}${NC}?"; then
				CONFIG_FILES_TO_INSTALL[$filename]=$(realpath "$file")
			fi
		fi
	done
}

function copy() {
	if [ -d $1 ]; then
		shopt -s dotglob
		echo -e "Do you want to copy file from '${BLUE}${1}${NC}':"
		for file in $1/*; do
			if [ -f "$file" ]; then
				filename=$(basename "$file")
				if ask "- copy ${YELLOW}${filename}${NC} to ${YELLOW}${2}${NC}?"; then
					cp $file $2
				fi
			fi
		done
	fi
}

# Ask if user wants to install apps (only if available)
if [ "$INSTALL_APPS" = true ]; then
	if ask "Do you want to install apps?"; then
		INSTALL_APPS=true
	else
		INSTALL_APPS=false
	fi
fi

# If user wants to install apps ask one by one
if [ "$INSTALL_APPS" = true ]; then
	for app in ${APPS[@]}; do
		if ask "Install ${YELLOW}$app${NC}"; then
			if [[ $app == "starship" ]]; then
				curl -sS https://starship.rs/install.sh | sh
			else
				sudo apt install $app
			fi
		fi
	done
fi


if ask "Do you want to copy config files?" ; then
	
	copy shell/home ~
	copy shell/home/.config ~/.config
	if [ "$IS_LINUX" = true ] && [ -d shell/linux/home ]; then
		copy shell/linux/home ~
		copy shell/linux/home/.config ~./config
	fi
	
	if [ "$IS_LINUX" = false ] && [ -d shell/windows/home ]; then
		copy shell/windows/home ~
		copy shell/windows/home/.config ~/.config
	fi
fi

if ask "Do you want to configure ${YELLOW}.bashrc${NC}?"; then
	SH="${HOME}/.bashrc"
	echo "#!/bin/bash" > $SH
	echo -e "\nConfiguring $SH file\n"

	arrVar=("AC" "TV" "Mobile" "Fridge" "Oven" "Blender")

	# Ask which files should be used - start with common configuration
	include "shell"
	
	if [ "$IS_LINUX" = true ] && [ -d shell/linux ]; then
		include "shell/linux"
	fi
	
	if [ "$IS_LINUX" = false ] && [ -d shell/windows ]; then
		include "shell/windows"
	fi
	
	for app in "${!CONFIG_FILES_TO_INSTALL[@]}"; do 
		cat ${CONFIG_FILES_TO_INSTALL["$app"]} >> "$SH"
	done | sort


	echo -e "\n${GREEN}Done!${NC} Now execute '${YELLOW}source ~/.bashrc${NC}' to reload!"
fi

# Support copying .files for example .vimrc


